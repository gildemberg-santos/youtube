"""
https://guihkx.github.io/ulist/
"""
import os
import sys
from argparse import ArgumentParser

try:
    import youtube_dl
except:
    print("Você não o modulo youtube-dl instalado no seu PC.")
    exit()

parser = ArgumentParser()
parser.add_argument("-f", "--fileinput", dest="filenameinput", help="write report to FILE", metavar="FILE")
parser.add_argument("-q", "--quiet", action="store_false", dest="verbose", default=True, help="don't print status messages to stdout")
args = parser.parse_args()

try:
    inputfile = args.__dict__['filenameinput']
    if not inputfile:
        assert False
except:
    print("Exemplo: [-h] [-f FILE] [-q]")
    print("Informe todos os parametos")
    exit()

try:
    if not os.path.exists(inputfile):
        assert False
except:
    print("O arquivo informado não existe!")
    exit()

with open(inputfile, 'r') as fr:
    dados = fr.readlines()
    os.system("mkdir Output")
for item in dados:
    '''
    Tenho que colocar mais um argumentos para alternar entre audio  video
    '''
    #audio
    #cmd = 'cd Output/ && youtube-dl {0} -x --audio-format "mp3"'.format(str(item).replace("\n",""))

    #video
    cmd = 'cd Output/ && youtube-dl {0}'.format(str(item).replace("\n",""))

    os.system(cmd)
exit()
